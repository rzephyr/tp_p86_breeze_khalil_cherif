"use strict";

const LATITUDE = 45.508320;
const LONGITUDE = -73.566431;

$(function () {
    let url = `http://api.openweathermap.org/data/2.5/weather?lat=${LATITUDE}&lon=${LONGITUDE}&&APPID=d372021858e26c181fc642ca0f0dbd18`;
    console.log(url);
    $.get(url, function (data, status) {
        console.log("Status: ", status);
        console.log('Réception des données AJAX', data);

        // Temperature
        let temperature_celcius = data.main.temp - 273.15;
        console.log(`Température : ${temperature_celcius.toFixed(2)}°C`);
        $("#meteo_actuel p.temperature").text(`En ce moment, il fait ${temperature_celcius.toFixed(2)}°C`);

        // Description du temps
        console.log(`Description du ciel : ${data.weather[0].description}`);
        $("#meteo_actuel p.description").text(`Description du temps : ${data.weather[0].description}`);

        // Icone du temps
        let id_icon = data.weather[0].icon;
        console.log(id_icon);
        $("#meteo_actuel img.icone").attr("src", `http://openweathermap.org/img/w/${id_icon}.png`);
    })
        .fail(function(){
            throw 'Erreur Ajax';
        });
});

